#include <Arduino.h>
#include <Wire.h>
#include <math.h>
#include <RadioLib.h>

#define USE_TIMER_1     false
#define USE_TIMER_2     false
#define USE_TIMER_3     true
#define USE_TIMER_4     false
#define USE_TIMER_5     false
#include <TimerInterrupt.h>

#include "packet.hpp"
#include "channel.hpp"
#include "motor.hpp"

// Uncomment to enable serial logging
#define DEBUG

void handle_timer(void);
void handle_receive(void);

const int rf_ce_pin = 4; // FIXME swapped
const int rf_cs_pin = 3;
const int rf_irq_pin = 2; // Only in HW version 2

nRF24 radio = new Module(rf_cs_pin, rf_irq_pin, rf_ce_pin);

const int led_pin = 13;

const int timeout_ms = 500;

int address_offset = 0x00;

MotorPins status_l {
  .alarm_in = ALARM_NORMAL,
  .speed_select_in = SPEED_SELECT_EXTERNAL,
  .direction_in = DIRECTION_CW,
  .brake_in = BRAKE_RUN,
  .start_in = START_STOP,
};

MotorPins status_r {
  .alarm_in = ALARM_NORMAL,
  .speed_select_in = SPEED_SELECT_EXTERNAL,
  .direction_in = DIRECTION_CW,
  .brake_in = BRAKE_RUN,
  .start_in = START_STOP,
};

MotorPins next_status_l {
  .alarm_in = ALARM_NORMAL,
  .speed_select_in = SPEED_SELECT_EXTERNAL,
  .direction_in = DIRECTION_CW,
  .brake_in = BRAKE_RUN,
  .start_in = START_STOP,
};

MotorPins next_status_r {
  .alarm_in = ALARM_NORMAL,
  .speed_select_in = SPEED_SELECT_EXTERNAL,
  .direction_in = DIRECTION_CW,
  .brake_in = BRAKE_RUN,
  .start_in = START_STOP,
};

volatile robot_packet staging_packet;
volatile robot_packet packet;

volatile bool packet_received = false;
volatile unsigned long last_packet_received = 0;

volatile bool timeout_expired = false;

void handle_receive(void) {
  if (packet_received) {
    // There is a packet waiting or being processed
    return;
  }

  packet_received = true;
}

void handle_timer() {
  unsigned long now = millis();
  if (now - last_packet_received > timeout_ms) {
    timeout_expired = true;
    packet_received = true;
  }
}

void setup() {
  #ifdef DEBUG
  Serial.begin(9600);
  Serial.println("Setting up...");
  #endif

  Wire.begin();

  byte addr[] = {0x69, 0x13, 0x37, 0xba, 0xbb};

  // HW v2 switches
  pinMode(8, INPUT_PULLUP);
  pinMode(9, INPUT_PULLUP);
  pinMode(10, INPUT_PULLUP);

  // if (digitalRead(8) == LOW) {
  if (true) { // Always HW V.2 because PCB doesn't have this connection
#ifdef DEBUG
    Serial.println("Hardware version: 2");
#endif

    pin_start_motor_l = 28;
    pin_start_motor_r = 34;

    pins_r.alarm_in = pin_start_motor_r + 1;
    pins_r.speed_select_in = pin_start_motor_r + 0;
    pins_r.direction_in = pin_start_motor_r + 3;
    pins_r.brake_in = pin_start_motor_r + 2;
    pins_r.start_in = pin_start_motor_r + 5;

    pins_l.alarm_in = pin_start_motor_l + 1;
    pins_l.speed_select_in = pin_start_motor_l + 0;
    pins_l.direction_in = pin_start_motor_l + 3;
    pins_l.brake_in = pin_start_motor_l + 2;
    pins_l.start_in = pin_start_motor_l + 5;

    set_addr(addr, digitalRead(9), digitalRead(10));
  } else {
#ifdef DEBUG
    Serial.println("Hardware version: 1");
#endif

    pinMode(0, INPUT_PULLUP);
    pinMode(1, INPUT_PULLUP);

    set_addr(addr, digitalRead(0), digitalRead(1));
  }

  pinMode(led_pin, OUTPUT);
  digitalWrite(led_pin, LOW);

  pinMode(pins_l.alarm_in, OUTPUT);
  pinMode(pins_l.speed_select_in, OUTPUT);
  pinMode(pins_l.direction_in, OUTPUT);
  pinMode(pins_l.brake_in, OUTPUT);
  pinMode(pins_l.start_in, OUTPUT);

  pinMode(pins_r.alarm_in, OUTPUT);
  pinMode(pins_r.speed_select_in, OUTPUT);
  pinMode(pins_r.direction_in, OUTPUT);
  pinMode(pins_r.brake_in, OUTPUT);
  pinMode(pins_r.start_in, OUTPUT);

  // Set up motor pins; HIGH = OFF, LOW = ON
  digitalWrite(pins_l.start_in, START_STOP); // Stop
  digitalWrite(pins_r.start_in, START_STOP); // Stop
  delay(10);
  digitalWrite(pins_l.alarm_in, ALARM_NORMAL);
  digitalWrite(pins_r.alarm_in, ALARM_NORMAL);
  delay(10);
  digitalWrite(pins_l.speed_select_in, SPEED_SELECT_EXTERNAL); // External
  digitalWrite(pins_r.speed_select_in, SPEED_SELECT_EXTERNAL); // External
  delay(10);
  digitalWrite(pins_l.direction_in, DIRECTION_CW); // CW
  digitalWrite(pins_r.direction_in, DIRECTION_CW); // CW
  delay(10);
  digitalWrite(pins_l.brake_in, BRAKE_RUN); // Run
  digitalWrite(pins_r.brake_in, BRAKE_RUN); // Run

#ifdef DEBUG
  Serial.print(F("[nRF24] Starting radio ... "));
#endif

  int state = radio.begin(
    channel_frequency,
    channel_data_rate,
    channel_power,
    channel_address_width
  );

  if(state == ERR_NONE) {
#ifdef DEBUG
    Serial.println(F("Radio setup!"));
#endif
  } else {
    Serial.print(F("failed setting up radio, code "));
    Serial.println(state);
    while(true);
    state = ERR_NONE;
  }

#ifdef DEBUG
    Serial.println(F("[nRF24] Attaching IRQ handler"));
#endif
  radio.setIrqAction(handle_receive);

  state = radio.setCrcFiltering();
#ifdef DEBUG
  Serial.print(F("[nRF24] Enable CRC..."));
  if(state == ERR_NONE) {
    Serial.println(F("OK"));
  } else {
    Serial.print(F("Failed: "));
    Serial.println(state);
    state = ERR_NONE;
  }
#endif

  // set transmit address
  // NOTE: address width in bytes MUST be equal to the
  //       width set in begin() or setAddressWidth()
  //       methods (5 by default)
  state = radio.setReceivePipe(0, addr);
#ifdef DEBUG
  Serial.print(F("[nRF24] Setting receive pipe..."));
#endif
  if(state == ERR_NONE) {
#ifdef DEBUG
    Serial.println(F("OK"));
#endif
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while(true);
  }

  state = radio.setAutoAck();
#ifdef DEBUG
  Serial.print(F("[nRF24] Enable Auto-ACK..."));
  if(state == ERR_NONE) {
    Serial.println(F("OK"));
  } else {
    Serial.print(F("Failed: "));
    Serial.println(state);
    state = ERR_NONE;
  }
#endif

#ifdef DEBUG
  Serial.print(F("[nRF24] Starting receive..."));
#endif
  state = radio.startReceive();
#ifdef DEBUG
  if(state == ERR_NONE) {
    Serial.println(F("OK"));
  } else {
    Serial.print(F("Failed: "));
    Serial.println(state);
    state = ERR_NONE;
  }
#endif

#ifdef DEBUG
  Serial.println(F("Attaching timeout handler..."));
#endif
  last_packet_received = millis(); // Interpret initial state as starting "zero packet"
  ITimer3.init();
  ITimer3.attachInterruptInterval(timeout_ms / 4, handle_timer);
}

void loop() {
  if (!packet_received) {
    delay(40);
    return;
  }

  if (timeout_expired) {
    Serial.println(F("Timeout expired"));
    packet.l_speed = 0;
    packet.r_speed = 0;
    timeout_expired = false;
  } else {
    int state = radio.readData((uint8_t *)&staging_packet, robot_packet_size);

    if (state == ERR_NONE) {
      if (staging_packet.signature == robot_packet_signature) {
        packet.l_cw = staging_packet.l_cw;
        packet.r_cw = staging_packet.r_cw;
        packet.l_speed = staging_packet.l_speed;
        packet.r_speed = staging_packet.r_speed;
#ifdef DEBUG
        char debugvalue[255];
        sprintf(debugvalue, "l_cw: %d; l_speed: %d; r_cw: %d; r_speed: %d",
                packet.l_cw, packet.l_speed, packet.r_cw, packet.r_speed);
        Serial.println(debugvalue);
#endif
      } else {
        // Cancel loop and wait for next packet
        packet_received = false;
        radio.startReceive();
        return;
      }
    } else if (state == ERR_RX_TIMEOUT) {
      packet_received = false;
      radio.startReceive();
      return;
    } else {
      // some other error occurred
      Serial.print(F("failed, code "));
      Serial.println(state);
      packet_received = false;
      radio.startReceive();
      return;
    }
  }

  last_packet_received = millis();

#ifdef DEBUG
  char debugvalue[255];
  sprintf(debugvalue,
          "l_cw: %d; l_speed: %d; r_cw: %d; r_speed: %d",
          packet.l_cw,
          packet.l_speed,
          packet.r_cw,
          packet.r_speed
  );
  Serial.println(debugvalue);
#endif

  // --- left ----

  // 1100 - address base
  // 000  - A2 A1 A0
  // last R/W bit is set by Wire.h
  Wire.beginTransmission(0b1100000);

  // 00   - C2 C1 - fast mode write
  // 00   - PD1 PD0 - normal power tmode
  // xxxx - 4 MSB of speed_r
  Wire.write(0b00001111 & (packet.l_speed >> 8));

  // xxxx xxxx - 8 LSB speed_r
  Wire.write(0b11111111 & packet.l_speed);
  Wire.endTransmission();

  if (packet.l_cw > 0) {
    next_status_l.direction_in = DIRECTION_CW;
  } else {
    next_status_l.direction_in = DIRECTION_CCW;
  }

  if (packet.l_speed < 16) {
    next_status_l.start_in = START_STOP;
  }
  next_status_l.start_in = START_RUN;

  // --- right ----

  // 1100 - address base
  // 000  - A2 A1 A0
  // last R/W bit is set by Wire.h
  Wire.beginTransmission(0b1100001);

  // 00   - C2 C1 - fast mode write
  // 00   - PD1 PD0 - normal power mode
  // xxxx - 4 MSB of speed_r
  Wire.write(0b00001111 & (packet.r_speed >> 8));

  // xxxx xxxx - 8 LSB speed_r
  Wire.write(0b11111111 & packet.r_speed);
  Wire.endTransmission();

  if (packet.r_cw > 0) {
    next_status_r.direction_in = DIRECTION_CW;
  } else {
    next_status_r.direction_in = DIRECTION_CCW;
  }

  if (packet.r_speed < 16) {
    next_status_r.start_in = START_STOP;
  } else {
  }
  next_status_r.start_in = START_RUN;



  update_motor_pins(
    &status_l, &next_status_l,
    &status_r, &next_status_r
  );

  digitalWrite(led_pin, LOW);

  packet_received = false;

  // Start listening again
  delay(50);
  digitalWrite(led_pin, HIGH);
  radio.startReceive();
}
