#include <Arduino.h>
#include <SPI.h>
#include <RadioLib.h>

#include "packet.hpp"
#include "channel.hpp"
#include "joystick.hpp"

#define sign(x) ((x) < 0 ? -1 : 1)

// Uncomment to enable more serial logging
// #define DEBUG
#define TRANSMIT

const uint8_t rf_ce_pin = 48;
const uint8_t rf_cs_pin = 47;
const uint8_t rf_irq_pin = 49;

// nRF24 has the following connections:
// CS pin:    10
// IRQ pin:   2
// CE pin:    3
nRF24 radio = new Module(rf_cs_pin, rf_irq_pin, rf_ce_pin);

// 0 = min speed, 3 = max speed
uint8_t speed_bits = 0;
uint8_t speed_x_divider = 1;

const uint8_t min_speed = 16;

const size_t n_switches = 10;
const uint8_t pins_switches[n_switches] = {
  30, 28, 26, 24, 22,
  31, 29, 27, 25, 23
};

bool joystick_2_is_unique = false;
bool joystick_3_is_unique = false;

bool flip_x = false;
bool flip_y = false;
bool swap_xy = false;

void setup() {
  Serial.begin(9600);

  Serial.println(F("Setting up joystick"));
  init_joystick(&joystick_1);
  init_joystick(&joystick_2);
  init_joystick(&joystick_3);

  Serial.println(F("Setting up switches"));
  for (size_t i = 0; i < n_switches; i++) {
    pinMode(pins_switches[i], INPUT_PULLUP);
  }

  // Joystick adjustments
  pinMode(68, INPUT_PULLUP);
  pinMode(69, INPUT_PULLUP);

  set_addr(
    joystick_1.addr,
    digitalRead(pins_switches[0]),
    digitalRead(pins_switches[1])
  );

  set_addr(
    joystick_2.addr,
    digitalRead(pins_switches[2]),
    digitalRead(pins_switches[3])
  );
  if (
    digitalRead(pins_switches[2]) !=
    digitalRead(pins_switches[0]) &&
    digitalRead(pins_switches[3]) !=
    digitalRead(pins_switches[1])
  ) {
    joystick_2_is_unique = true;
  }

  set_addr(
    joystick_3.addr,
    digitalRead(pins_switches[4]),
    digitalRead(pins_switches[5])
  );
  if (
    digitalRead(pins_switches[4]) !=
    digitalRead(pins_switches[0]) &&
    digitalRead(pins_switches[5]) !=
    digitalRead(pins_switches[1]) &&
    digitalRead(pins_switches[4]) !=
    digitalRead(pins_switches[2]) &&
    digitalRead(pins_switches[5]) !=
    digitalRead(pins_switches[3])
  ) {
    joystick_3_is_unique = true;
  }

  Serial.print(F("[nRF24] Starting radio ... "));
  int state = radio.begin(
    channel_frequency,
    channel_data_rate,
    channel_power,
    channel_address_width
  );
  if(state == ERR_NONE) {
    Serial.println(F("Radio setup!"));
  } else {
    Serial.print(F("failed setting up radio, code "));
    Serial.println(state);
    while(true);
  }

  state = radio.setCrcFiltering();
  Serial.print(F("[nRF24] Enable CRC..."));
  if(state == ERR_NONE) {
    Serial.println(F("OK"));
  } else {
    Serial.print(F("Failed: "));
    Serial.println(state);
  }

}

void transmit(robot_packet *packet, byte *addr) {
  int state;

  Serial.print(F("[nRF24] Transmitting packet ... "));

  state = radio.setTransmitPipe(addr);
  if (state != ERR_NONE) {
    Serial.print(F("failed to change transmit pipe, code "));
    Serial.println(state);
    return;
  }

  state = radio.transmit((uint8_t *)packet, robot_packet_size, 0);

  if (state == ERR_NONE) {
    // the packet was successfully transmitted
    Serial.println(F("sent packet!"));

  } else if (state == ERR_PACKET_TOO_LONG) {
    // the supplied packet was longer than 32 bytes
    Serial.println(F("too long!"));

  } else if (state == ERR_ACK_NOT_RECEIVED) {
    // acknowledge from destination module
    // was not received within 15 retries
    Serial.println(F("ACK not received!"));

  } else if (state == ERR_TX_TIMEOUT) {
    // timed out while transmitting
    Serial.println(F("timeout!"));

  } else {
    // some other error occurred
    Serial.print(F("failed, code "));
    Serial.println(state);
  }
}

void read_speed_settings() {
  speed_bits = 0;
  if (digitalRead(pins_switches[6]) == LOW) {
    speed_bits += 1;
  }
  if (digitalRead(pins_switches[7]) == LOW) {
    speed_bits += 2;
  }

  speed_x_divider = 1;
  if (digitalRead(pins_switches[8]) == LOW) {
    speed_x_divider *= 2;
  }
  if (digitalRead(pins_switches[9]) == LOW) {
    speed_x_divider *= 2;
  }

  flip_x = digitalRead(67) == LOW;
  flip_y = digitalRead(68) == LOW;
  swap_xy = digitalRead(69) == LOW;

#ifdef DEBUG
  Serial.print("speed: ");
  Serial.print(speed_bits);
#endif
}

void read_joystick_values(Joystick *joystick, robot_packet *packet) {
  read_speed_settings();

  packet->l_cw = 0;
  packet->l_speed = 0;
  packet->r_cw = 0;
  packet->r_speed = 0;

  // Read joystick position (10 bit) unsigned
  uint16_t joystick_x_value = 1023 - analogRead(joystick->pin_x);
  uint16_t joystick_y_value = analogRead(joystick->pin_y);

  if (swap_xy) {
    joystick_y_value = 1023 - analogRead(joystick->pin_x);
    joystick_x_value = analogRead(joystick->pin_y);
  }
  if (flip_x) {
    joystick_x_value = 1023 - joystick_x_value;
  }
  if (flip_y) {
    joystick_y_value = 1023 - analogRead(joystick->pin_y);
  }

  // Calculate motor speeds
  double value_x = ((double)joystick_x_value - 512) / 512; // becomes 9-bit signed
  double value_y = ((double)joystick_y_value - 512) / 512; // becomes 9-bit signed

  value_x /= speed_x_divider;

  double beta = (sq(value_y) - sq(value_x)) / (sq(value_y) + sq(value_x));

  double alpha = 0;
  double atan_yx = atan(value_x != 0 ? (value_y / value_x) : 0);
  // Q1: 0 -> PI/2
  // Q2: -PI/2 -> 0
  // Q3: 0 -> PI/2
  // Q4: -PI/2 -> 0

  if (atan_yx < (-PI/4)) {
    alpha = value_y;
  } else if (atan_yx < 0) {
    alpha = value_x;
  } else if (atan_yx < PI/4) {
    alpha = value_x;
  } else if (atan_yx < PI/2) {
    alpha = value_y;
  }

  // 10-bit combined values
  int16_t value_l = 0;
  int16_t value_r = 0;

  if (abs(value_x) <= 0.02) {
#ifdef DEBUG
    Serial.print("; Y ");
#endif
    // On y-axis
    value_l = value_y * 512;
    value_r = value_y * 512;
  } else if (abs(value_y) <= 0.02) {
#ifdef DEBUG
    Serial.print("; X ");
#endif
    value_l = value_x * 512;
    value_r = -value_x * 512;
  } else if (value_x >= 0 && value_y >= 0) {
#ifdef DEBUG
    Serial.print("; Q1");
#endif
    value_l = alpha * 512;
    value_r = beta * 512;
  } else if (value_x <= 0 && value_y >= 0) {
#ifdef DEBUG
    Serial.print("; Q2");
#endif
    value_l = beta * 512;
    value_r = abs(alpha * 512);
  } else if (value_x <= 0 && value_y <= 0) {
#ifdef DEBUG
    Serial.print("; Q3");
#endif
    value_l = -abs(alpha * 512);
    value_r = -beta * 512;
  } else if (value_x >= 0 && value_y <= 0) {
#ifdef DEBUG
    Serial.print("; Q4");
#endif
    value_l = -beta * 512;
    value_r = -abs(alpha * 512);
  }

  #ifdef DEBUG
  Serial.print("; value_x: ");
  Serial.print(value_x);
  Serial.print("; value_y: ");
  Serial.print(value_y);
  Serial.print("; alpha: ");
  Serial.print(alpha);
  Serial.print("; beta: ");
  Serial.print(beta);
  Serial.print("; atan_yx: ");
  Serial.print(atan_yx);
  Serial.print("; value_l: ");
  Serial.print(value_l);
  Serial.print("; value_r: ");
  Serial.print(value_r);
  Serial.println();
  #endif

  byte direction_l = value_l > 0 ? 0 : 1;
  unsigned int speed_l = min(abs(value_l) << speed_bits, (1 << 13) - 1); // 9-bit signed -> 10-bit unsigned
  if (speed_l < min_speed) {
    speed_l = 0;
  }

  byte direction_r = value_r > 0 ? 0 : 1;
  unsigned int speed_r = min(abs(value_r) << speed_bits, (1 << 13) - 1); // 9-bit signed -> 10-bit unsigned
  if (speed_r < min_speed) {
    speed_r = 0;
  }

  packet->l_cw = direction_l;
  packet->l_speed = speed_l;
  packet->r_cw = direction_r;
  packet->r_speed = speed_r;
}

struct robot_packet packet {
  .signature = robot_packet_signature,
  .l_cw = 0,
  .l_speed = 0,
  .r_cw = 0,
  .r_speed = 0
};

void loop() {
  packet.l_cw = 0;
  packet.l_speed = 0;
  packet.r_cw = 0;
  packet.r_speed = 0;

#ifdef DEBUG
  Serial.println("Joystick 1:");
#endif

  read_joystick_values(&joystick_1, &packet);

  #ifdef DEBUG
  char debugvalue[255];
  sprintf(debugvalue,
          "l_cw: %d; l_speed: %d; r_cw: %d; r_speed: %d",
          packet.l_cw,
          packet.l_speed,
          packet.r_cw,
          packet.r_speed
  );
  Serial.println(debugvalue);
  #endif

#ifdef TRANSMIT
  transmit(&packet, joystick_1.addr);
#endif

  if (joystick_2_is_unique) {
#ifdef DEBUG
  Serial.println("Joystick 2:");
#endif

  read_joystick_values(&joystick_2, &packet);

  #ifdef DEBUG
  sprintf(debugvalue,
          "l_cw: %d; l_speed: %d; r_cw: %d; r_speed: %d",
          packet.l_cw,
          packet.l_speed,
          packet.r_cw,
          packet.r_speed
  );
  Serial.println(debugvalue);
  #endif

#ifdef TRANSMIT
  transmit(&packet, joystick_2.addr);
#endif
  }

  if (joystick_3_is_unique) {
#ifdef DEBUG
    Serial.println("Joystick 3:");
#endif

    read_joystick_values(&joystick_3, &packet);

#ifdef DEBUG
    sprintf(debugvalue, "l_cw: %d; l_speed: %d; r_cw: %d; r_speed: %d",
            packet.l_cw, packet.l_speed, packet.r_cw, packet.r_speed);
    Serial.println(debugvalue);
#endif

#ifdef TRANSMIT
    transmit(&packet, joystick_3.addr);
#endif
  }
}
