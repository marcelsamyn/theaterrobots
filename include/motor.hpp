#ifndef __MOTOR_H_
#define __MOTOR_H_

#include <Arduino.h>

#define ALARM_NORMAL HIGH
#define ALARM_RESET LOW
#define SPEED_SELECT_INTERNAL LOW
#define SPEED_SELECT_EXTERNAL HIGH
#define DIRECTION_CW LOW
#define DIRECTION_CCW HIGH
#define BRAKE_RUN LOW
#define BRAKE_STOP HIGH
#define START_RUN LOW
#define START_STOP HIGH

typedef struct MotorPins {
  int alarm_in;
  int speed_select_in;
  int direction_in;
  int brake_in;
  int start_in;
} MotorPins;

const MotorPins motor_offsets = {
  .alarm_in = 0,
  .speed_select_in = 1,
  .direction_in = 2,
  .brake_in = 3,
  .start_in = 4
};

int pin_start_motor_l = 34;
MotorPins pins_l {
  .alarm_in = pin_start_motor_l + motor_offsets.alarm_in,
  .speed_select_in = pin_start_motor_l + motor_offsets.speed_select_in,
  .direction_in = pin_start_motor_l + motor_offsets.direction_in,
  .brake_in = pin_start_motor_l + motor_offsets.brake_in,
  .start_in = pin_start_motor_l + motor_offsets.start_in
};

// HW v1 configuration

int pin_start_motor_r = 28;
MotorPins pins_r {
  .alarm_in = pin_start_motor_r + motor_offsets.alarm_in,
  .speed_select_in = pin_start_motor_r + motor_offsets.speed_select_in,
  .direction_in = pin_start_motor_r + motor_offsets.direction_in,
  .brake_in = pin_start_motor_r + motor_offsets.brake_in,
  .start_in = pin_start_motor_r + motor_offsets.start_in
};

#define UPDATE_PIN(PIN_NAME) \
    if (old_l->PIN_NAME != new_l->PIN_NAME) { \
        digitalWrite(pins_l.PIN_NAME, new_l->PIN_NAME); \
        old_l->PIN_NAME = new_l->PIN_NAME; \
        changed = true; \
    } \
    if (old_r->PIN_NAME != new_r->PIN_NAME) { \
        digitalWrite(pins_r.PIN_NAME, new_r->PIN_NAME); \
        old_r->PIN_NAME = new_r->PIN_NAME; \
        changed = true; \
    } \
    if (changed) { \
        delay(10); \
        changed = false; \
    }

/**
 * Updates all motor pins. Will only change pins that are different, as the
 * motor needs 10ms between input changes. Updates pipelined by batching L and R
 * together.
 */
void update_motor_pins(
    MotorPins *old_l,
    MotorPins *new_l,
    MotorPins *old_r,
    MotorPins *new_r
) {
    bool changed = false;

    UPDATE_PIN(brake_in)
    UPDATE_PIN(direction_in)
    UPDATE_PIN(start_in)
    UPDATE_PIN(speed_select_in)
    UPDATE_PIN(alarm_in)
}

#endif // __MOTOR_H_
