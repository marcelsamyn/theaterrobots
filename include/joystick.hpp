#ifndef __JOYSTICK_H_
#define __JOYSTICK_H_

#include <Arduino.h>

typedef struct Joystick {
    int pin_x;
    int pin_y;
    int pin_button;
    int value_x;
    int value_y;
    byte addr[5];
} Joystick;

Joystick joystick_1 {
    .pin_x = A1,
    .pin_y = A0,
    .pin_button = 44,
    .value_x = 0,
    .value_y = 0,
    .addr = {0x69, 0x13, 0x37, 0xba, 0xbb}
};

Joystick joystick_2 {
    .pin_x = A3,
    .pin_y = A2,
    .pin_button = 45,
    .value_x = 0,
    .value_y = 0,
    .addr = {0x69, 0x13, 0x37, 0xba, 0xbc}
};

Joystick joystick_3 {
    .pin_x = A5,
    .pin_y = A4,
    .pin_button = 46,
    .value_x = 0,
    .value_y = 0,
    .addr = {0x69, 0x13, 0x37, 0xba, 0xbd}
};

void init_joystick(Joystick *joystick) {
    pinMode(joystick->pin_x, INPUT);
    pinMode(joystick->pin_y, INPUT);
    pinMode(joystick->pin_button, INPUT);
}

#endif // __JOYSTICK_H_
