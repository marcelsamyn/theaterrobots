#include <Arduino.h>

#ifndef __PACKET_H_
#define __PACKET_H_

struct robot_packet {
    uint16_t signature;
    byte l_cw;
    unsigned int l_speed;
    byte r_cw;
    unsigned int r_speed;
};

const size_t robot_packet_size = 2 + 1 + 2 + 1 + 2;
const uint16_t robot_packet_signature = 0x4c;

#endif // __PACKET_H_
