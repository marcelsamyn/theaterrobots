#ifndef __CHANNEL_H_
#define __CHANNEL_H_

#include <Arduino.h>

const int16_t channel_frequency = 2520;
const int16_t channel_data_rate = 1000;
const int8_t channel_power = 0;
const uint8_t channel_address_width = 5;

void set_addr(byte *addr, int value0, int value1) {
    byte address_offset = 0xbb;

    // LOW = switch enabled (input_pullup)
    if (value0 == LOW) {
      address_offset += 2;
    }
    if (value1 == LOW) {
      address_offset += 1;
    }

    addr[0] = 0x69;
    addr[1] = 0x13;
    addr[2] = 0x37;
    addr[3] = 0xba;
    addr[4] = address_offset;

    Serial.print(F("  Set address with offset "));
    Serial.println(address_offset);
}

#endif // __CHANNEL_H_
